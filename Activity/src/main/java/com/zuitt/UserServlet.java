package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class UserServlet extends HttpServlet {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1804323570948502389L;
	
	public void init() throws ServletException{
		System.out.println("***************************************");
		System.out.println(" UserServlet has been initialized ");
		System.out.println("***************************************");
	}
	
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException{
		
		
		//System properties
		String fname = req.getParameter("fname");
		System.getProperties().put("fname", fname );
		String lname = req.getParameter("lname");
		
		//Http session
		HttpSession Lastname = req.getSession();
		Lastname.setAttribute("lname", lname);
		
	
		// Servlet Context getattr
		
		
		String email = req.getParameter("email");
		ServletContext srvContext = getServletContext();
		srvContext.setAttribute("email", email);
		
		//URL sendRedirect
		 String contact = req.getParameter("contact");
		 res.sendRedirect("details?contact="+contact);			
	}
		 public void destroy() {
	     System.out.println("***********************************");
	     System.out.println(" UserServlet has been destroyed ");
	     System.out.println("***********************************");
	}
}
