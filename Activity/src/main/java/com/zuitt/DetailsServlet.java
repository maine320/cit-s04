package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class DetailsServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4484146589817098769L;

	public void init() throws ServletException{
		System.out.println("***************************************");
		System.out.println(" DetailsServlet has been initialized ");
		System.out.println("***************************************");
	}
	
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException{
		
		//branding
		ServletContext srvContext = getServletContext();
	    String branding = srvContext.getInitParameter("branding");
	    
		//System properties
		String fname = System.getProperty("fname");
		
		//Http session
		
		HttpSession session = req.getSession();
		String lname = session.getAttribute("lname").toString();
		
	
		// Servlet Context getattr
		
		ServletContext context = getServletContext();
		String email = context.getAttribute("email").toString();
		
		//URL sendRedirect
		 String contact = req.getParameter("contact");
				
		PrintWriter out = res.getWriter();
		out.println("<h1>Welcome to " + branding + "</h1>");
		out.println("<p> First Name: " + fname + "</p>" +
                "<p> Last Name: " + lname + "</p>" +
                "<p> Contact: " + contact + "</p>" +
                "<p> Email: " + email + "</p>");	
	}
	
		public void destroy() {
        System.out.println("************************************");
        System.out.println(" DetailsServlet has been destroyed ");
        System.out.println("************************************");
    }
}
